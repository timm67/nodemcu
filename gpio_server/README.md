Note: Joe was talking about wiring a physical pull-down resistor in with the switch in the Switch Web Service portion of the lesson. Note that most micros have this pull-up or pull-down as part of the internal pin structure on the chip.

micropython lets you control it like so:

```>>> pin = machine.Pin(0, machine.Pin.IN, machine.Pin.PULL_DOWN)```

To properly do switches (Joe may touch on this) you should also address 'debouncing' as the switch voltage value takes time to settle to a steady state value. This is a basic subject, so just google "switch debouncing"; there are many ways to do it in software

FYI, the switch code should look like the following ('+' instead of '.' ). Use the internal PULL_DOWN if you're pulling the signal up to 5V or 3.3V by closing the switch. Also, convert the 0 or 1 integer value to a string. Same thing for ADC exercise (the response body definition).

```
switch_pin = machine.Pin(10, machine.Pin.IN, machine.Pin.PULL_DOWN)

def switch():
     body = "{state: " + str(switch_pin.value()) + "}"
     return response_template % body
```
