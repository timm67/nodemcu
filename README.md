# nodemcu

I used an ESP32 SoC based board for this exercise. The ESP32 is the 
successor to the ESP8266, and has more memory and a 240 MHz dual-core 
application processor. Here is the pinout:

![ESP32 Pinout](./esp32-pinout.jpg)

## Flashing micropython

Here is how I flashed micropython to it:

```
Tims-MacBook-Pro:Downloads timm$ esptool.py --chip esp32 --port /dev/cu.SLAB_USBtoUART erase_flash

Tims-MacBook-Pro:Downloads timm$ esptool.py --chip esp32 --port /dev/cu.SLAB_USBtoUART --baud 460800 write_flash -z 0x1000 esp32-20190409-v1.10-271-g74ed06828.bin
esptool.py v2.6
Serial port /dev/cu.SLAB_USBtoUART
Connecting........___
Chip is ESP32D0WDQ5 (revision 1)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
MAC: b4:e6:2d:d9:e0:51
Uploading stub...
Running stub...
Stub running...
Changing baud rate to 460800
Changed.
Configuring flash size...
Auto-detected Flash size: 4MB
Compressed 1146496 bytes to 717231...
Wrote 1146496 bytes (717231 compressed) at 0x00001000 in 17.2 seconds (effective 532.6 kbit/s)...
Hash of data verified.

Leaving...
```
## Grading

The code in this repo cannot be run on an esp8266 microcontroller; it must be run on an ESP32. If you need to borrow my hardware to grade the assignment, please let me know. Thanks!
