# Analog measurements (ADC) and pseudo-analog output (PWM)

## ADC measurements

I connected the supplied photocell in series with a 10K Ohm resistor 
to ground. Note that I'm using the ESP32, the successor to the esp8266. 
It only measures between 0 and 3.3V, but provides more granularity
with 4096 ADC counts vs 1024 for the 8266. 

I turned an LED desklamp on and off and was able to swing the ADC value
from just above 500 counts to full scale, a pretty good range of 
operation. When the desklamp was on, the ADC count was pegged at 4095,
meaning that the photocell was most conductive when the light was 
brightest

![Dim environment -- lower ADC count .](./low.png)

![Light environment -- higher ADC count .](./high.png)

## PWM Output

The scale for duty cycle of a PWM output on the ESP32 is from 0 to 1023,
with 1023 being a duty cycle of 100%. The following pictures show a duty
cycle on the LED of 25% and 75% respectively. It's hard to see because 
the LED is green (not the best choice as human eyes are very sensitive to
green), but you can see a brightness difference. At 75%, the LED lights
up the back of the DHT22 sensor it is in front of. 

![25 percent duty cycle.](./duty25.jpg)

![75 percent duty cycle .](./duty75.jpg)

