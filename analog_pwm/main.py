
try:
    import usocket as socket
except:
    import socket

response_404 = """HTTP/1.0 404 NOT FOUND

<h1>404 Not Found</h1>
"""

response_500 = """HTTP/1.0 500 INTERNAL SERVER ERROR

<h1>500 Internal Server Error</h1>
"""

response_template = """HTTP/1.0 200 OK

%s
"""

import machine
import ntptime, utime
from machine import RTC
from time import sleep

rtc = RTC()
try:
    seconds = ntptime.time()
except:
    seconds = 0
# For some reason, this is throwing an exception: 
# Overflow Error: overflow converting long int to machine word
# rtc.datetime(utime.localtime(seconds))

# Pin 34 on ESP32 is channel 6 on ADC 0
photo_adc = machine.ADC(machine.Pin(34))

# Using Pin 23 for PWM; init to 128 Hz and half duty cycle
# NOTE: cannot use GPIO pins 34-39 on the ESP32 for PWM out
led_pwm = machine.PWM(machine.Pin(23))
led_pwm.freq(128) 
led_pwm.duty(512) 

def time():
    body = """<html>
<body>
<h1>Time</h1>
<p>%s</p>
</body>
</html>
""" % str(rtc.datetime())

    return response_template % body

#
# For Analog to digital conversion on the ESP32, we
# can only convert up to 3.3V, and this will be 
# converted with between 0 and 4095 ADC counts
#

def light():
     body = "{value: " + str(photo_adc.read()) + "}"
     return response_template % body

#
# For PWM on the ESP32, duty cycle is based on 0 to 1023,
# so 512 is 50% duty cycle. So 25% duty cycle (dim) is 256, 
# and a 75% duty cycle (bright) is 768. 
#

DIM_DUTY_CYCLE=128
BRIGHT_DUTY_CYCLE=896

def dim():
    led_pwm.duty(DIM_DUTY_CYCLE)
    body = "{PWM duty cycle: " + str(DIM_DUTY_CYCLE) + "}"
    return response_template % body

def bright():
    led_pwm.duty(BRIGHT_DUTY_CYCLE)
    body = "{PWM duty cycle: " + str(BRIGHT_DUTY_CYCLE) + "}"
    return response_template % body
 
def dummy():
    body = "This is a dummy endpoint"

    return response_template % body

# Routing dictionary -- call a method based on path
handlers = {
    'time'   : time,
    'dummy'  : dummy,
    'light'  : light, 
    'dim'    : dim, 
    'bright' : bright, 
}

def main():
    s = socket.socket()
    ai = socket.getaddrinfo("0.0.0.0", 8080)
    addr = ai[0][-1]

    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind(addr)
    s.listen(5)
    print("Listening, connect your browser to http://<this_host>:8080")

    while True:
        sleep(1)
        res = s.accept()
        client_s = res[0]
        client_addr = res[1]
        req = client_s.recv(4096)
        print("Request:")
        print(req)

        try:
            path = req.decode().split("\r\n")[0].split(" ")[1]
            handler = handlers[path.strip('/').split('/')[0]]
            response = handler()
        except KeyError:
            response = response_404
        except Exception as e:
            response = response_500
            print(str(e))

        client_s.send(b"\r\n".join([line.encode() for line in response.split("\n")]))

        client_s.close()
        print()

main()
